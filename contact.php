 <div id="contact-page" class="container">
        <div class="bg">
            <div class="row">           
                <div class="col-sm-12">                         
                    <h2 class="title text-center">Contacto <strong>EC</strong></h2>                                                      
          <div class="col-sm-12">
            <div class="address">
             <img src="images/home/contacto.png" alt="" />
              <p><center>Quito-Ecuador</center></p>
            </div>
          </div>
                </div>                  
            </div>      
            <div class="row">   
                <div class="col-sm-8">
                    <div class="contact-form">
                        <h2 class="title text-center">Ponerse en contacto</h2>
                        <div class="status alert alert-success" style="display: ninguno"></div>
                        <form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
                            <div class="form-group col-md-6">
                                <input type="text" name="name" class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="subject" class="form-control" required="required" placeholder="Dirección">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Su mensaje aquí"></textarea>
                            </div>                        
                            <div class="form-group col-md-12">
                                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-info">
                        <h2 class="title text-center">Datos de contacto</h2>
                        <address>
                            <p>Papeleria los gemelos</p>
                            <p>2020</p>
                            <p>Quito-Ecuador</p>
                            <p>Teléfono celular: +593 95 978 4910</p>
                             <p>Email: pgemelos@gmail.com</p>
                        </address>
                        <div class="social-networks">
                            <h2 class="title text-center">Redes sociales</h2>
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/alex.tocagon.7"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/AlexKacike1997"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#https://www.gmail.com/mail/help/intl/es/about.html?iframe"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCmr1RF8JhLm5t0b0wEh1NXw"><i class="fa fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>              
            </div>  
        </div>  
    </div><!--/#contact-page-->